const filter = require('../../assets/images/filter.png')
const arrow = require('../../assets/images/right-arrow.png')
const left_arrow = require('../../assets/images/left-arrow.png')
const filter_active = require('../../assets/images/filter-2.png')


const Icons = {
    filter, arrow, left_arrow, filter_active
};

export default Icons;
