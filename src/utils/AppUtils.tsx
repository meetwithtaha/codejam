import React, {  } from 'react';

import { View, Dimensions, StyleSheet } from 'react-native'

var { height, width } = Dimensions.get('window')

export const separator = () => { return <View style={styles.seprator} /> }

var styles = StyleSheet.create({
    seprator: {
        width: width,
        height: width / 50,
    },
})