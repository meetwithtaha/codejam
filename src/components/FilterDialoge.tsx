import React, { useState, memo } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { Dialog } from 'react-native-simple-dialogs';
import { getUsers } from '../network/ServerRequest';
import { separator } from '../utils/AppUtils';

type FilterDialogeProps = {
    onSelectedUser(userId: string): void
    onTouchOutside(): void,
    showDialoge: boolean
}

const FilterDialoge = ({ onSelectedUser, showDialoge, onTouchOutside }: FilterDialogeProps) => {

    let { loading, data, error, noData, } = getUsers()



    const renderItem = ({ item }) => (
        <TouchableOpacity style={{ borderBottomWidth: .2, borderBottomColor: "#ccc", paddingVertical: 3, }}
            onPress={() => {
                onSelectedUser(item.id)
                onTouchOutside()
            }}>

            <Text>{item.name}</Text>
            <Text style={{ color: "#95a5a6" }}>{item.email}</Text>

        </TouchableOpacity>
    )

    return (

        <Dialog
            visible={showDialoge}
            onTouchOutside={onTouchOutside}>
            <View>

                {loading &&
                    <ActivityIndicator />
                }


                {!loading && data &&
                    <FlatList
                        data={data}
                        renderItem={renderItem}
                        ItemSeparatorComponent={separator}
                        keyExtractor={(item, index) => index.toString()}
                    />}


            </View>
        </Dialog>
    )
}

export default memo(FilterDialoge)

const styles = StyleSheet.create({
    title: {
        fontSize: 30,
        fontWeight: "bold",
        flex: 1
    },
    container: {
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: "row",
        alignItems: "center"
    },
    filterImage: {
        width: 25, height: 25, resizeMode: "contain"
    }
})