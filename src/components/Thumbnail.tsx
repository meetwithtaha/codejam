import React, { memo } from 'react';
import { StyleSheet, Animated, Image, ViewStyle } from 'react-native';

type ThumbnailProps = {
    uri: string
    style?: ViewStyle<ViewStyle>
}

const Thumbnail = ({ uri, style }: ThumbnailProps) => {

    return (
        <Animated.Image
            style={[styles.thumbnail, style]}
            source={{ uri: uri }}
            resizeMode={'cover'} />
    )
}

export default memo(Thumbnail)

const styles = StyleSheet.create({
    thumbnail: {
        width: 65, height: 65, resizeMode: "contain", borderRadius: 7
    },

})