import React from 'react';
import { View, Platform, StatusBar } from 'react-native';

interface propTypes {
    dark?: boolean,
    bgColor?: string
}

const AppStatusbar = ({ dark, bgColor }: propTypes) => {

    let isAndroid = Platform.OS == "android" ? true : false;

    if (isAndroid) {
        return (
            <StatusBar style={'light'} backgroundColor={bgColor} />
        )
    } else {
        return <View style={{ backgroundColor: bgColor }}>
            <StatusBar style={'light'} />
        </View>
    }
}

export default AppStatusbar