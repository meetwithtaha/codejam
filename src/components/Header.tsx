import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import AppStatusbar from './AppStatusbar';
import { Icons } from '../utils';

type HeaderProps = {
    title: string,
    onPress(): void
    filterActive: boolean
}

const Header = ({ title, onPress, filterActive }: HeaderProps) => {

    return (
        <SafeAreaView style={styles.container}>
            <AppStatusbar />

            <Text style={styles.title}>{title}</Text>

            <TouchableOpacity onPress={onPress}>

                <Image
                    source={filterActive ? Icons.filter_active : Icons.filter}
                    style={styles.filterImage}
                />

            </TouchableOpacity>

        </SafeAreaView>
    )
}

export default Header

const styles = StyleSheet.create({
    title: {
        fontSize: 30,
        fontWeight: "bold",
        flex: 1
    },
    container: {
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: "row",
        alignItems: "center"
    },
    filterImage: {
        width: 25, height: 25, resizeMode: "contain"
    }
})