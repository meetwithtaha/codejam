import React, { useRef, useState, memo, useEffect } from 'react'
import {
    StyleProp, Image, Dimensions, ViewStyle, TouchableOpacity,
    View, Animated, Modal, TouchableWithoutFeedback, Platform, Linking
} from 'react-native'
import store from '../store/AppStore'
import Icons from '../utils/Icons'

let { width, } = Dimensions.get('window')

const height = Dimensions.get('window').height / 2

type ImageSliderProps = {

    containerStyle?: StyleProp<ViewStyle>
    images: any[]
    onChangeIndex(index: number): void

}

var ImageSlider = ({ images, onChangeIndex }: ImageSliderProps) => {

    // alert('----')

    var useFlatlist = useRef()

    const SPACING = 0;
    const ITEM_SIZE = width;
    const EMPTY_ITEM_SIZE = (width - ITEM_SIZE) / 2;

    images = [{ key: "1", extra: true }, ...images, { key: "2", extra: true }]

    let imageRefs: any[] = []

    const scrollX = React.useRef(new Animated.Value(0)).current;

    return <View style={{ height }}>

        <Animated.FlatList
            showsHorizontalScrollIndicator={false}
            data={images ? images : [{ loading: true }]}
            keyExtractor={(item: any) => JSON.stringify(item)}
            horizontal
            scrollEnabled={false}
            scroll={false}
            ref={useFlatlist}
            bounces={false}
            decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
            renderToHardwareTextureAndroid
            contentContainerStyle={{ alignItems: 'center' }}
            snapToInterval={ITEM_SIZE}
            snapToAlignment='start'
            onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                { useNativeDriver: false }
            )}
            scrollEventThrottle={16}
            renderItem={({ item, index }: any) => {

                if (item.extra) {
                    return <View key={item.key} style={{ width: EMPTY_ITEM_SIZE }} />;
                }

                const inputRange = [
                    (index - 2) * ITEM_SIZE,
                    (index - 1) * ITEM_SIZE,
                    index * ITEM_SIZE,
                ];

                const translateY = scrollX.interpolate({
                    inputRange,
                    outputRange: [.5, 1, .5],
                    extrapolate: 'clamp',
                });


                return (
                    <View style={{ width: ITEM_SIZE, }}>
                        <Animated.View
                            style={{
                                overflow: 'hidden',
                                marginHorizontal: SPACING,
                                height: height,
                                alignItems: 'center',
                                transform: [{ scale: translateY }],
                                backgroundColor: 'white',
                                flexDirection: "row",
                            }}>

                            <Image
                                ref={(view) => {
                                    imageRefs[index] = view
                                }}
                                source={{ uri: item.url }}
                                style={{
                                    width: ITEM_SIZE, height: height, resizeMode: 'cover',
                                    margin: 0,
                                    marginBottom: 10,
                                }}
                            />

                            {index > 1 &&
                                <ChangeImageButton
                                    style={{ position: 'absolute', left: 10, }}
                                    type="revert"
                                    onPress={() => {
                                        if (index > 0) {
                                            index = index - 1
                                            store.dispatch({ type: 'onChange', currentIndex: index })
                                            useFlatlist.current.scrollToIndex({ animated: true, index: index })
                                            onChangeIndex(index)
                                        }
                                    }}
                                />}


                            <ChangeImageButton
                                style={{ position: 'absolute', right: 10, }}
                                type="forward"
                                onPress={() => {
                                    var newIndex = index + 1
                                    store.dispatch({ type: 'onChange', currentIndex: newIndex })
                                    useFlatlist.current.scrollToIndex({ animated: true, index: newIndex })
                                    onChangeIndex(newIndex)
                                }}
                            />




                        </Animated.View>
                    </View>
                );
            }}
        />


    </View>

}

export default memo(ImageSlider)


const ChangeImageButton = ({ type, onPress, style }) => {


    return (<TouchableOpacity
        style={style}
        onPress={onPress}>

        <Image
            source={type == "forward" ? Icons.arrow : Icons.left_arrow}
            style={{
                width: 30, height: 30, resizeMode: 'cover',
            }}
        />

    </TouchableOpacity >)
}