
import ApiClient from './ApiClient'
import { useState, useEffect } from 'react';

export const getAlbums = async () => {

    let response: any = await Promise.all([
        await ApiClient.get('/albums'),
        await ApiClient.get('/photos'),
        await ApiClient.get('/users'),
    ]).catch(err => console.log('error in album => ' + err))

    // console.log('A L B U M - R E S P O N S E => '+JSON.stringify(response))

    let album = response[0];
    let phots = response[1];
    let users = response[2];

    let newAlbum = album.map((element) => {

        let albumPhotos = phots.filter((p) => p.albumId === element.id)
        let albumOwner = users.find((u) => u.id === element.userId)

        element['phots'] = albumPhotos
        element['owner'] = albumOwner
        return element

    })

    return newAlbum

}

// export const getUsers = async () => {
//     const response = await ApiClient.get('/users')
//     return response;
// }

export const getUsers = () => {

    const [state, setState] = useState({ loading: true, data: undefined, error: null, noData: false, })

    useEffect(() => {

        setState({ loading: true, data: undefined, error: null, noData: false, })

        ApiClient.get('/users').then((response: []) => {

            if (response.length > 0) {
                setState({ loading: false, data: response, error: null, noData: false, })
            } else {
                setState({ loading: false, data: undefined, error: null, noData: true, })
            }

        }).catch(e => {
            console.log(e)
            setState({ loading: false, data: undefined, error: e, noData: false, })
        })


    }, [])

    return state
}