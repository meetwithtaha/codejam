

export const BASE_URL = "https://jsonplaceholder.typicode.com"


class Api {

    post = (endPoint: string, data?: FormData) => {

        let executer = async (resolve: any, reject: any) => {

            try {

                var myHeaders = new Headers();
                myHeaders.append("Accept", "application/json");
                // myHeaders.append("Cookie", "cookie_here");

                var requestOptions: RequestInit = {}

                if (data != undefined) {
                    requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        body: data,
                        redirect: 'follow'
                    };
                }else {
                    requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        redirect: 'follow'
                    };
                }

                var url = BASE_URL + endPoint

                let response = await fetch(url, requestOptions)
                let json = await response.json()
                resolve(json)

            } catch (e) {
                reject(e)
            }

        }
        var promise = new Promise(executer);

        return promise;
    }

    get = (end_point: any, params?: any) => {
        var url = BASE_URL + end_point
        if (params != undefined && params != "") {
            url += '?' + params
        }

        console.log(url)

        let executer = async (resolve: any, reject: any) => {

            try {

                var myHeaders = new Headers();
                myHeaders.append("Accept", "application/json");
                // myHeaders.append("Cookie", "cookie_here");

              
                var requestOptions: RequestInit = {
                    method: 'GET',
                    headers: myHeaders,
                    redirect: 'follow'
                };


                let response = await fetch(url, requestOptions);
                let body = await response.json();

                resolve(body)

            } catch (e) {
                reject(e)
            }

        }


        var promise = new Promise(executer);

        return promise;
    }

}
const api = new Api();
export default api;