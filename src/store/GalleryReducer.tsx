const initialState : any = {
  currentIndex: 1,
}

const GalleryReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'onChange':
      return { currentIndex: action.currentIndex }
  }
  return state
}

export default GalleryReducer