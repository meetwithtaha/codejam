import { createStore } from 'redux'
import GalleryReducer from './GalleryReducer'

const store = createStore(GalleryReducer)

export default store