import React, { useEffect, useState } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import { Header, FilterDialoge } from '../components';
import { Icons } from '../utils';
import { getAlbums } from '../network/ServerRequest';
import { separator } from '../utils/AppUtils';
var { height, width } = Dimensions.get('window')

const Albums = ({ navigation }: any) => {

    const [albumList, setAlbum] = useState([])
    const [albumListOriginal, setAlbumOriginal] = useState([])
    const [showDialoge, setShowDialoge] = useState(false)
    const [filterActive, setFilterActive] = useState(false)

    const onPress = () => {
        if (filterActive) {

            setAlbum(albumListOriginal)
            setFilterActive(false)

        } else setShowDialoge(true)
    }

    let executeAlbumRequest = async () => {
        let response = await getAlbums()
        setAlbum(response)
        setAlbumOriginal(response)
    }

    useEffect(() => {
        executeAlbumRequest()
    }, [])

    const renderItem = ({ item }) => {

        let thumnail = item.phots.length > 0 ? item.phots[0].thumbnailUrl : 'https://via.placeholder.com/150'

        const onPress = () => {
            navigation.navigate('Gallery', { album: item })
        }

        return (
            <TouchableOpacity activeOpacity={.8} style={styles.renderItem} onPress={onPress}>

                <Image
                    style={styles.thumbnail}
                    source={{ uri: thumnail }}
                    resizeMode={'cover'} />

                <View style={styles.albumsTextContainer}>
                    <Text numberOfLines={2} style={styles.albumTitle}>{item.title}</Text>

                    <Text numberOfLines={1} style={styles.albumOwner}>{item.owner.name}</Text>
                </View>

                <Image
                    style={styles.arrow}
                    source={Icons.arrow}
                    resizeMode={'cover'} />


            </TouchableOpacity>
        );
    };

    const onSelectedUser = (userId: string) => {
        let albumPhotos = albumListOriginal.filter((p) => p.userId === userId)
        setAlbum(albumPhotos)
        setFilterActive(true)
    }

    const onTouchOutside = () => {
        setShowDialoge(false)
    }


    return (
        <View style={{ flex: 1 }}>

            <Header title="Albums" filterActive={filterActive} onPress={onPress} />

            <FlatList
                data={albumList}
                renderItem={renderItem}
                ItemSeparatorComponent={separator}
                keyExtractor={(item, index) => index.toString()}
            />

            <FilterDialoge
                onTouchOutside={onTouchOutside}
                showDialoge={showDialoge}
                onSelectedUser={onSelectedUser} />

        </View>
    )
}

export default Albums

var styles = StyleSheet.create({

    thumbnail: {
        width: 65, height: 65, resizeMode: "contain", borderRadius: 7
    },
    renderItem: {
        flexDirection: "row", alignItems: "center", paddingHorizontal: 10
    },
    albumsTextContainer: {
        marginLeft: 10, flex: 1
    },
    albumTitle: {
        fontSize: 16, marginRight: 7
    },
    albumOwner: {
        fontSize: 15, color: "#707070", marginTop: 5
    },
    arrow: {
        width: 15, height: 15, resizeMode: "contain", marginRight: 5
    }
})