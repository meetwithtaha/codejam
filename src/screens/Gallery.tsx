import React, { useState, useRef, useEffect } from 'react';
import { View, Image, FlatList, Animated, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import { ImageSlider, Header, AppStatusbar, Thumbnail } from '../components';
import { separator } from '../utils/AppUtils';
import { useSelector } from 'react-redux';
import store from '../store/AppStore';
const { height, width } = Dimensions.get('window')

const itemWidth = (width / 2) - 2
const imageWidth = (width / 2.2)
const ITEM_SIZE = width;


const Gallery = ({ route }: any) => {

    var useFlatlist = useRef()
    let album = route.params.album
    var currentIndex = useSelector((state: any) => state.currentIndex)
    // var [currentIndex, setIndex] = useState(0)

    const renderItem = ({ item, index }) => {

        return <Animated.View style={[styles.itemContainer]}>

            <Thumbnail uri={item.thumbnailUrl} style={[styles.thumbnail, {
                borderWidth: (currentIndex - 1) === index ? 3 : 0,
                borderColor: "orange"
            }]} />

        </Animated.View>
    }


    useEffect(() => {

        return () => {
            store.dispatch({ type: 'onChange', currentIndex: 1 })
        }

    }, [])

    // const onIndex = (index) => {
        // index = index - 1
        // useFlatlist.current.scrollToIndex({ animated: true, index: (index) })

        // if (index % 2 != 0 && index >= 3) {
        //     useFlatlist.current.scrollToIndex({ animated: true, index: (index) })
        // }
    // }

    const getItemLayout = (data, index) => {
        return { length: 80, offset: 80 * (index), index };
    }


    return (
        <View style={{ flex: 1 }}>


            <ImageSlider
                images={album.phots}
                onChangeIndex={() => {}}
            />

            <FlatList
                numColumns={2}
                ref={useFlatlist}
                contentContainerStyle={{ justifyContent: "space-between", paddingBottom: 100 }}
                data={album.phots}
                renderItem={renderItem}
                ItemSeparatorComponent={separator}
                keyExtractor={(item, index) => index.toString()}
                // getItemLayout={getItemLayout}
            />

        </View>



    )
}

export default Gallery

var styles = StyleSheet.create({

    itemContainer: { width: itemWidth, alignItems: 'center', marginHorizontal: 1, marginTop: 10, },
    thumbnail: { width: imageWidth, height: imageWidth, resizeMode: "contain" }
})