import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer, } from '@react-navigation/native';
import { createStackNavigator, } from '@react-navigation/stack';
import { Albums, Gallery } from './src/screens'
import { Provider as StoreProvider } from 'react-redux'
import store from './src/store/AppStore'

const Stack = createStackNavigator();

export default function App() {
  return (

    <StoreProvider store={store}>

      <NavigationContainer>

        <Stack.Navigator>

          <Stack.Screen name="Albums" component={Albums}
            options={options}
          />
          <Stack.Screen name="Gallery" component={Gallery}
            options={options}
          />

        </Stack.Navigator>

      </NavigationContainer>

    </StoreProvider>

  );
}

const options = () => ({
  headerShown: false,
})
